# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-22 12:54
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0002_auto_20160613_1809'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='approved',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='product',
            name='photo',
            field=models.FileField(default=1, upload_to=b''),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='cart',
            name='token',
            field=models.CharField(default=b'qIem77n56hWrDwpE6WJUQHBylju3RYCiU8oTCMuv', max_length=40),
        ),
        migrations.AlterField(
            model_name='cart',
            name='user',
            field=models.ForeignKey(blank=True, default=b'no', on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='product',
            name='category',
            field=models.ManyToManyField(blank=True, to='shop.Category'),
        ),
        migrations.AlterField(
            model_name='product',
            name='seller',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='shop.Seller'),
        ),
    ]
