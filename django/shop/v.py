from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.core.urlresolvers import reverse
from paypal.standard.forms import PayPalPaymentsForm

# -*- coding: utf-8 -*-

def add(request):
    return HttpResponse("Added")
 

def error404(request): 
     return render(request,'shop/404.html')

def main(request):
    items = [
        {'id': 1, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 2, 'item_name': "����� ��� �����", 'item_price': 1999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
    ]
    categories = [
        {'id': 1, 'name': "������ �� �������"},
        {'id': 2, 'name': "�������"},
        {'id': 3, 'name': "������"},
        {'id': 4, 'name': "���������"},
    ]
    return render(request, 'shop/main.html', {'items': items, 'categories': categories})


def profile(request):
    items = [
        {'id': 1, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 2, 'item_name': "����� ��� �����", 'item_price': 1999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
    ]
    categories = [
        {'id': 1, 'name': "������ �� �������"},
        {'id': 2, 'name': "�������"},
        {'id': 3, 'name': "������"},
        {'id': 4, 'name': "���������"},
    ]
    return render(request, 'shop/profile.html', {'items': items, 'categories': categories})
    
def newproduct(request):
    items = [
        {'id': 1, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 2, 'item_name': "����� ��� �����", 'item_price': 1999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
    ]
    categories = [
        {'id': 1, 'name': "������ �� �������"},
        {'id': 2, 'name': "�������"},
        {'id': 3, 'name': "������"},
        {'id': 4, 'name': "���������"},
    ]
    return render(request, 'shop/add-prod.html', {'items': items, 'categories': categories})
   
def basket(request):
    items = [
        {'id': 1, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 2, 'item_name': "����� ��� �����", 'item_price': 1999.99},
    ]
    categories = [
        {'id': 1, 'name': "������ �� �������"},
        {'id': 2, 'name': "�������"},
    ]
    return render(request, 'shop/basket.html', {'items': items, 'categories': categories})
    
def catalog(request):
    items = [
        {'id': 1, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 2, 'item_name': "����� ��� �����", 'item_price': 1999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
    ]
    categories = [
        {'id': 1, 'name': "������ �� �������"},
        {'id': 2, 'name': "�������"},
        {'id': 3, 'name': "������"},
        {'id': 4, 'name': "���������"},
    ]
    return render(request, 'shop/num-prod.html', {'items': items, 'categories': categories})
    
def product(request):
    items = [
        {'id': 0, 'item_name': "�������� �����", 'item_price': 999.99, 'discr': "�������-������� �������� �� ������������� ������ � �������������� �������� ������������� ���������. ������� ��������-������� ������� ����������� �����. ����������� � ��������� ����� ��� ������� � ����������))) � ��� �� ������ ����������)))", 'item_author': "Matilda",},
    ]
    categories = [
        {'id': 1, 'name': "������ �� �������"},
        {'id': 2, 'name': "�������"},
        {'id': 3, 'name': "������"},
        {'id': 4, 'name': "���������"},
    ]
    return render(request, 'shop/prod.html', {'items': items, 'categories': categories})

def signin(request):
    items = [
        {'id': 1, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 2, 'item_name': "����� ��� �����", 'item_price': 1999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
        {'id': 3, 'item_name': "�������� �����", 'item_price': 999.99},
    ]
    categories = [
        {'id': 1, 'name': "������ �� �������"},
        {'id': 2, 'name': "�������"},
        {'id': 3, 'name': "������"},
        {'id': 4, 'name': "���������"},
    ]
    return render(request, 'shop/signin.html', {'items': items, 'categories': categories})
    
def login(request):
    if request.user.is_authenticated():
        return HttpResponse("{0} <a href='/accounts/logout'>exit</a>".format(request.user))
    else:
        return HttpResponse("<a href='/login/vk-oauth2/'>login with VK</a>")


@login_required
def account_profile(request):
    return HttpResponse("Hi, {0}! Nice to meet you.".format(request.user.first_name))


def account_logout(request):
    logout(request)
    return HttpResponseRedirect('/')
   
    
@csrf_exempt
def paypal_success(request):
    """
    Tell user we got the payment.
    """
    return HttpResponse("Money is mine. Thanks.")

@login_required
def paypal_pay(request):
    """
    Page where we ask user to pay with paypal.
    """
    paypal_dict = {
        "business": "nastya.fadeewa-facilitator_api1.yandex.ru",
        "amount": "100.00",
        "currency_code": "RUB",
        "item_name": "products in socshop",
        "invoice": "INV-00001",
        "notify_url": reverse('paypal-ipn'),
        "return_url": "http://localhost:8000/payment/success/",
        "cancel_return": "http://localhost:8000/payment/cart/",
        "custom": str(request.user.id)
    }

    # Create the instance.
    form = PayPalPaymentsForm(initial=paypal_dict)
    context = {"form": form, "paypal_dict": paypal_dict}
    return render(request, "shop/payment.html", context)

