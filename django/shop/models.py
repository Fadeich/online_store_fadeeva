from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django import forms
import random
import string
from django.forms import ModelForm


class NameForm1(forms.Form):
    name = forms.CharField(label='name', max_length=100)
    description = forms.CharField(label='description', max_length=100) #, widget=forms.Textarea
    price = forms.DecimalField(max_digits=6, decimal_places=2, label='price')
    photo = models.FileField(upload_to='')


def generate_token():
    token = "".join(random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for x in range(40))
    return token


class Seller(models.Model):
    name = models.CharField(max_length=30, default="nana")
    user = models.OneToOneField(User, default = 1)
    information = models.TextField(blank=True)

    def __str__(self):
        return self.user_id


class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name
    
    def get_url(self):
        return "/catalog/"+str(self.id)+"/"


class Cart(models.Model):
    token = models.CharField(max_length=40, default=generate_token())
    total_price = models.DecimalField(max_digits=6, decimal_places=2, default=0)
    user = models.ForeignKey(User, blank=True, default="no")
    archive = models.BooleanField(default=False)
    date_added = models.TimeField(default=timezone.now)

    def add_product(self, id_product, q):
        prod=Product.objects.get(id=id_product)
        if len(ProductCart.objects.filter(product=prod, cart=self).values_list()):
            pc = ProductCart.objects.get(product=prod, cart=self)
            prod = pc.product
            pc.quantity += q
            pc.price_for_products += prod.price*q
            self.total_price += prod.price*q
            pc.save()
            self.save()
        else:
            prod = Product.objects.get(id=id_product)
            pc = ProductCart.objects.create(cart=self, product=prod, price_for_products=prod.price*q, quantity=q)
            self.total_price += prod.price * q
            pc.save()
            self.save()

    def return_token(self):
        return self.token

    def get_pay_url(self):
        return "/payment/cart/" + str(self.id) + "/"


class Product(models.Model):
    name = models.CharField(max_length=300)
    description = models.TextField(blank=True)
    photo = models.FileField(upload_to='') #uploads/%Y/%m/%d/
    price = models.DecimalField(max_digits=6, decimal_places=2)
    publication_date = models.DateTimeField(default=timezone.now)
    seller = models.ForeignKey(User, blank=True)
    category = models.ManyToManyField(Category, blank=True)
    approved = models.BooleanField(default=False)
    #cart = models.ManyToManyField(Cart, through="ProductCart")

    def __str__(self):
        return self.name

    def get_url(self):
        return "/product/" + str(self.id) + "/"

    def get_seller_url(self):
        print("ha")
        return "/profile/" + str(self.seller_id) + "/"

    def publish(self):
        self.approved=True
        self.save()

    def get_url_photo(self):
        return "/media/" + str(self.photo)

class NameForm(ModelForm):
    class Meta:
        model = Product
        fields = ['name', 'description', 'price', 'photo']
        labels = {
            'name': 'Name',
            'description': 'Description',
            'price' : 'Price',
            'photo' : 'Photo'
        }




class ProductCart(models.Model):
    cart = models.ForeignKey(Cart)
    product = models.ForeignKey(Product)
    price_for_products = models.DecimalField(max_digits=6, decimal_places=2, default=0)
    quantity=models.IntegerField(default=0)

    def prod_name(self):
        return str(self.product_name)


class Order(models.Model):
    sum_in_dollars = models.DecimalField(max_digits=10, decimal_places=2)
    buyer = models.ForeignKey(User)




class Payment(models.Model):
    sum_in_dollars = models.DecimalField(max_digits=10, decimal_places=2)
    date_of_payment = models.DateField(auto_now=False, auto_now_add=True)
    success = models.BooleanField()
    buyer = models.ForeignKey(User)
    order = models.OneToOneField(Order, primary_key=True)  # primary_key?




    



