
from django.contrib import admin
from .models import Product, Seller, Category, Cart, ProductCart


admin.site.register(Seller)
admin.site.register(Cart)
admin.site.register(ProductCart)
admin.site.register(Category)
admin.site.register(Product)