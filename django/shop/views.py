#!/usr/bin/python
# -*- coding: utf-8 -*- 
from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import HttpRequest
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.core.urlresolvers import reverse
from paypal.standard.forms import PayPalPaymentsForm
from django.utils import timezone
from .models import Product, Category, Seller, Cart, ProductCart, NameForm
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.core.files.base import ContentFile
from django.core.cache import cache


@login_required
@csrf_exempt
def your_name(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request: photo=form.cleaned_data['photo'],
        form = NameForm(request.POST, request.FILES)
        # check whether it's valid:
        if form.is_valid():
            # form.save()
            prod = Product.objects.create(name=form.cleaned_data['name'],  price=form.cleaned_data['price'], description=form.cleaned_data['description'], seller=request.user)
            # file_content = ContentFile(request.FILES['photo'].read())
            # prod.photo.save(request.FILES['photo'].name, file_content)
            prod.photo = request.FILES['photo']
            prod.save()
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect('/newproduct/')

    # if a GET (or any other method) we'll create a blank form
    else:
        print("get")
        form = NameForm()
    return render(request, 'name.html', {'form': form})


def delete(request):
    pid = request.POST.get('item')[4:]
    pc = ProductCart.objects.get(id=pid)
    ca = pc.cart
    ca.total_price -= pc.price_for_products
    ca.save()
    pc.delete()
    return HttpResponse("Deleted")


def get_or_create_cart_auth(request):
    if 'cart' in request.session:
        token_of_cart = request.session['cart']
        c = Cart.objects.get(token=token_of_cart)
        c.user = request.user
        c.save()
    else:
        Cart.objects.create(user=request.user)
    return request


def return_cart(request):
    if request.user.is_authenticated():
        if len(Cart.objects.filter(user=request.user, archive=False).values_list()):
            return request, Cart.objects.filter(user=request.user, archive=False).order_by('date_added')[0]
        else:
            return request, Cart.objects.create(user=request.user)
    else:
        if 'cart' in request.session:
            token_of_cart = request.session['cart']
            print(token_of_cart)
            cc = Cart.objects.get(token=token_of_cart)
            return request, cc
        else:
            u = User.objects.get(id=1)
            cart2 = Cart.objects.create(user=u)
            request.session['cart'] = cart2.return_token()
            return request, cart2


def add(request):
    i = request.POST.get('item')[4:]
    print(i)
    if len(Product.objects.filter(id=i).values_list()):
        req, cart = return_cart(request)
        cart.add_product(i, 1)
        return HttpResponse("Added")
 

def error404(request): 
     return render(request,'shop/404.html')


def main(request):
    #products = cache.get_or_set('products', Category.objects.all(), 10)
    #products = cache.get_or_set('books', Book.objects.all(), 10)
    items = Product.objects.filter(approved=True).order_by('name')
    new_items = Product.objects.filter(approved=True).order_by('publication_date')
    categories = Category.objects.all()
    return render(request, 'shop/main.html', {'items': items, 'categories': categories, 'new_items' : new_items})


def product(request, num):
    items = Product.objects.filter(id=num, approved=True)
    categories = Category.objects.all()
    return render(request, 'shop/prod.html', {'items': items, 'categories': categories})

    
def catalog(request, num):
    items = Product.objects.filter(category__id=num, approved=True)
    categories = Category.objects.all()
    cat = Category.objects.filter(id=num)
    return render(request, 'shop/num-prod.html', {'items': items, 'categories': categories, 'cat' : cat})


def profile(request, num):
    items = Product.objects.filter(seller__id=num, approved=True)
    categories = Category.objects.all()
    seller = Seller.objects.filter(id=num)
    return render(request, 'shop/profile.html', {'items': items, 'categories': categories, 'seller' : seller})


def newproduct(request):
    categories = Category.objects.all()
    return render(request, 'shop/add-prod.html', {'categories': categories})


def basket(request):
    categories = Category.objects.all()
    req, c = return_cart(request)
    pc1 = ProductCart.objects.filter(cart=c)
    #pc = ProductCart.objects.filter(cart=c).values_list()
    #items = []
    #for i in range(len(pc)):
        #print(Product.objects.get(id=pc[i][2]).name)
        #items.append([[Product.objects.get(id=pc[i][2]).name], [int(pc[i][3])], [pc[i][4]]])
    print("hey")
    #print(ProductCart.objects.filter(cart=c).values_list())  , 'items': items
    return render(req, 'shop/basket.html', {'categories': categories, 'prod': pc1, 'sum': float(c.total_price), 'car': c})


    
def signin(request):
    categories = Category.objects.all()
    return render(request, 'shop/signin.html', {'categories': categories})


def login(request):
    if request.user.is_authenticated():
        return HttpResponse("{0} <a href='/accounts/logout'>exit</a>".format(request.user))
    else:
        return HttpResponse("<a href='/login/vk-oauth2/'>login with VK</a>")


@login_required
def account_profile(request):
    return HttpResponseRedirect('/')


def account_logout(request):
    logout(request)
    if 'cart' in request.session:
        tok = request.session.pop('cart')
        c = Cart.objects.get(token=tok[1]).delete()
        ProductCart.objects.filter(cart=c).delete()
    return HttpResponseRedirect('/')
   
    
@csrf_exempt
def paypal_success(request, num):
    car = Cart.objects.get(id=num)
    car.archive=True
    car.save()
    """
    Tell user we got the payment.
    """
    return HttpResponse("Money is mine. Thanks.")

@login_required
def paypal_pay(request, num):
    print("here")
    car = Cart.objects.get(id=num)
    print("there")
    """
    Page where we ask user to pay with paypal.
    """
    paypal_dict = {
        "business": "nastya.fadeewa-facilitator@yandex.ru",
        "amount": str(car.total_price),
        "currency_code": "RUB",
        "item_name": "products in socshop",
        "invoice": "INV-"+str(car.id),
        "notify_url": reverse('paypal-ipn'),
        "return_url": "http://localhost:443/payment/success/"+str(num)+"/",
        "cancel_return": "http://localhost:443/payment/cart/",
        "custom": str(request.user.id)
    }

    # Create the instance.
    form = PayPalPaymentsForm(initial=paypal_dict)
    context = {"form": form, "paypal_dict": paypal_dict}
    return render(request, "shop/payment.html", context)



