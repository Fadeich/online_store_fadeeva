from django.conf.urls import include, url
from . import views
import social.apps.django_app.urls
import paypal.standard.ipn.urls
from django.views.generic.base import RedirectView
from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', views.main, name='main'),
    url(r'^profile/(?P<num>\d+)/$', views.profile, name='profile'),
    url(r'^newproduct/$', views.newproduct, name='newproduct'),
    url(r'^basket/$', views.basket, name='basket'),
    url(r'^product/(?P<num>\d+)/$', views.product, name='product'),
    url(r'^signin/$', views.signin, name='signin'),
    url(r'^catalog/(?P<num>\d+)/$', views.catalog, name='catalog'),
    url(r'^add-to-cart/$', views.add, name='add'),
    url(r'^delete/$', views.delete, name='delete'),
    url(r'^your_name/$', views.your_name, name='your_name'),
url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^accounts/logout/$', views.account_logout, name='logout'),
    url(r'^accounts/login/$', views.login, name='login'),
    url(r'^accounts/profile/$', views.account_profile, name='ac_profile'),
    url('', include(social.apps.django_app.urls, namespace='social')),
    url(r'^payment/cart/(?P<num>\d+)/$', views.paypal_pay, name='cart'),
    url(r'^payment/success/(?P<num>\d+)/$', views.paypal_success, name='success'),
    url(r'^paypal/', include(paypal.standard.ipn.urls)),
    url(r'^favicon\.ico$', RedirectView.as_view(url='/media/img/favicon.ico')),
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = 'mysite.views.my_custom_page_not_found_view'
